package cucumberTets.steps;

import com.jayway.restassured.RestAssured;
import com.jayway.restassured.response.ValidatableResponse;
import com.jayway.restassured.specification.RequestSpecification;
import cucumber.api.Scenario;
import cucumber.api.java.Before;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;


public class testSteps {

    private RequestSpecification request;
    private ValidatableResponse response;

    @Before
    public void before(Scenario scenario) {
        request = RestAssured.with();
    }


    @Given("^I build a API end point$")
    public void i_build_a_API_end_point(){
        request.given()

                .baseUri("http://www.localhost:port/healthcheck?pretty=true");

    }

    @When("^I call the service$")
    public void i_call_the_service(){
        response = request.get().then();


    }

    @Then("^I get the \"([^\"]*)\" response$")
    public void i_get_the_response(Integer arg1) {
        response.statusCode(arg1);


    }


    @Given("^I am at given$")
    public void i_am_at_given()  {
        System.out.println("At Given");
    }

    @When("^I am at when$")
    public void i_am_at_when() {
        System.out.println("At When");

    }

    @Then("^I can see then$")
    public void i_can_see_then() {
        System.out.println("At Then");

    }

}
