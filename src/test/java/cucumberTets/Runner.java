package cucumberTets;
import cucumber.api.CucumberOptions;
import org.junit.runner.RunWith;
import cucumber.api.junit.Cucumber;


@RunWith(Cucumber.class)

@CucumberOptions(format = { "html:target/cucumber-reports/html", "json:target/cucumber-reports/cucumber.json" }, glue = "cucumberTets.steps", features = "src/test/Cucumber")
public class Runner {



}